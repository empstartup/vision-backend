const express = require("express");
const path = require("path");
const logger = require("morgan");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const cors = require("cors");

const apiRoutes = require("./routes/api");

// for support method put, delete, and others via form view
const methodOverride = require("method-override");

const app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());

// override with POST having ?_method=some_method
app.use(methodOverride("_method"));

app.use("/api/auth", apiRoutes.authorizationRoutes);
app.use("/api/asignaturas", apiRoutes.asignaturaRoutes);
app.use("/api/colorReferencias", apiRoutes.colorReferenciaRoutes);
app.use("/api/competenciasGenericas", apiRoutes.competenciaGenericaRoutes);
app.use("/api/empresas", apiRoutes.empresaRoutes);
app.use("/api/objetivos", apiRoutes.objetivoRoutes);
app.use("/api/perfiles", apiRoutes.perfilRoutes);
app.use("/api/roles", apiRoutes.rolRoutes);
app.use("/api/taxonomias", apiRoutes.taxonomiaRoutes);
app.use("/api/tipoEnsenanzas", apiRoutes.tipoEnsenanzaRoutes);
app.use("/api/tipoReferencias", apiRoutes.tipoReferenciaRoutes);
app.use("/api/ejes", apiRoutes.ejeRoutes);
app.use("/api/empleados", apiRoutes.empleadoRoutes);
app.use("/api/nivelTipoEnsenanzas", apiRoutes.nivelTipoEnsenanzaRoutes);
app.use("/api/subEjes", apiRoutes.subEjeRoutes);
app.use("/api/usuarios", apiRoutes.usuarioRoutes);
app.use("/api/saberes", apiRoutes.saberRoutes);
app.use("/api/niveles", apiRoutes.nivelRoutes);
app.use("/api/nivelAsignaturas", apiRoutes.nivelAsignaturaRoutes);
app.use("/api/empleadoRoles", apiRoutes.empleadoRolRoutes);
app.use("/api/referencias", apiRoutes.referenciaRoutes);
app.use("/api/archivos", apiRoutes.archivoRoutes);
app.use("/api/indicadores", apiRoutes.indicadorRoutes);
app.use("/api/menus", apiRoutes.menuRoutes);
app.use("/api/secuencias", apiRoutes.secuenciaRoutes);
app.use(
  "/api/seguimientoCompetencias",
  apiRoutes.seguimientoCompetenciasRoutes
);
app.use("/api/competencias", apiRoutes.competenciasRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  throw err;

  // render the error page
  res.status(err.status || 500);
  res.json({ error: err.message });
});

module.exports = app;
