const archivoService = require("../services/archivo");

module.exports = {
  all(req, res) {
    archivoService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    archivoService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const { nombre, extension, archivoRelacionId } = req.body;

    archivoService.create(
      {
        Nombre: nombre,
        Extension: extension,
        Archivo_Relacion_ID: archivoRelacionId
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Archivo creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const { nombre, extension, archivoRelacionId } = req.body;
    const { id } = req.params;

    archivoService.update(
      {
        Nombre: nombre,
        Extension: extension,
        Archivo_Relacion_ID: archivoRelacionId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Archivo actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    archivoService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Archivo eliminado exitosamente."
      });
    });
  }
};
