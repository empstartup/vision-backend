const asignaturaService = require("../services/asignatura");

module.exports = {
  all(req, res) {
    asignaturaService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    asignaturaService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const { codigo, nombre, descripcion, estado } = req.body;

    asignaturaService.create(
      {
        codigo,
        nombre,
        descripcion,
        estado
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Asignatura creada exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const { codigo, nombre, descripcion, estado } = req.body;
    const { id } = req.params;

    asignaturaService.update(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Estado: estado
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Asignatura actualizada exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    asignaturaService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Asignatura eliminada exitosamente."
      });
    });
  }
};
