const colorReferenciaService = require("../services/colorReferencia");

module.exports = {
  all(req, res) {
    colorReferenciaService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    colorReferenciaService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const { codigo, nombre } = req.body;

    colorReferenciaService.create(
      {
        Codigo: codigo,
        Nombre: nombre
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Color referencia creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const { codigo, nombre } = req.body;
    const { id } = req.params;

    colorReferenciaService.update(
      {
        Codigo: codigo,
        Nombre: nombre
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Color referencia actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    colorReferenciaService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Color referencia eliminado exitosamente."
      });
    });
  }
};
