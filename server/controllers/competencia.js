const competenciaService = require("../services/competencia");

module.exports = {
  all(req, res) {
    competenciaService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    competenciaService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  getByNivelAsignatura(req, res) {
    const { nivelAsignaturaId } = req.params;

    competenciaService.getByNivelAsignatura(
      nivelAsignaturaId,
      (error, result) => {
        if (error) throw error;

        return res.status(200).json(result);
      }
    );
  },

  create(req, res) {
    const {
      codigo,
      nombre,
      descripcion,
      horas,
      fechaDesde,
      fechaHasta,
      estado,
      competenciaGenericaId,
      nivelAsignaturaId
    } = req.body;

    competenciaService.create(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Horas: horas,
        FechaDesde: fechaDesde,
        FechaHasta: fechaHasta,
        Estado: estado,
        CompetenciaGenerica_ID: competenciaGenericaId,
        NivelAsignatura_ID: nivelAsignaturaId
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Competencia creada exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const {
      codigo,
      nombre,
      descripcion,
      horas,
      fechaDesde,
      fechaHasta,
      estado,
      competenciaGenericaId,
      nivelAsignaturaId
    } = req.body;
    const { id } = req.params;

    competenciaService.update(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Horas: horas,
        FechaDesde: fechaDesde,
        FechaHasta: fechaHasta,
        Estado: estado,
        CompetenciaGenerica_ID: competenciaGenericaId,
        NivelAsignatura_ID: nivelAsignaturaId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Competencia actualizada exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    competenciaService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Competencia eliminada exitosamente."
      });
    });
  }
};
