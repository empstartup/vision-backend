const competenciaGenericaService = require("../services/competenciaGenerica");

module.exports = {
  all(req, res) {
    competenciaGenericaService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    competenciaGenericaService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const { codigo, nombre, descripcion } = req.body;

    competenciaGenericaService.create(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Competencia genérica creada exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const { codigo, nombre, descripcion } = req.body;
    const { id } = req.params;

    competenciaGenericaService.update(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Competencia genérica actualizada exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    competenciaGenericaService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Competencia genérica eliminada exitosamente."
      });
    });
  }
};
