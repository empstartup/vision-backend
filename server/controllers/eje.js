const ejeService = require("../services/eje");

module.exports = {
  all(req, res) {
    ejeService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    ejeService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const { codigo, nombre, descripcion, estado, asignaturaId } = req.body;

    ejeService.create(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Estado: estado,
        Asignatura_ID: asignaturaId
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Eje creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const { codigo, nombre, descripcion, estado, asignaturaId } = req.body;
    const { id } = req.params;

    ejeService.update(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Estado: estado,
        Asignatura_ID: asignaturaId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Eje actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    ejeService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Eje eliminado exitosamente."
      });
    });
  }
};
