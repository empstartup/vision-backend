const empleadoService = require("../services/empleado");

module.exports = {
  all(req, res) {
    empleadoService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    empleadoService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const {
      nombre,
      apellidoPaterno,
      apellidoMaterno,
      edad,
      rut,
      sexo,
      direccion,
      telefonoUno,
      telefonoDos,
      email,
      valido,
      empresaId
    } = req.body;

    empleadoService.create(
      {
        Nombre: nombre,
        Paterno: apellidoPaterno,
        Materno: apellidoMaterno,
        Edad: edad,
        Rut: rut,
        Sexo: sexo,
        Direccion: direccion,
        Telefono1: telefonoUno,
        Telefono2: telefonoDos,
        Email: email,
        Valido: valido,
        Empresa_ID: empresaId
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Empleado creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const {
      nombre,
      apellidoPaterno,
      apellidoMaterno,
      edad,
      rut,
      sexo,
      direccion,
      telefonoUno,
      telefonoDos,
      email,
      valido,
      empresaId
    } = req.body;
    const { id } = req.params;

    empleadoService.update(
      {
        Nombre: nombre,
        Paterno: apellidoPaterno,
        Materno: apellidoMaterno,
        Edad: edad,
        Rut: rut,
        Sexo: sexo,
        Direccion: direccion,
        Telefono1: telefonoUno,
        Telefono2: telefonoDos,
        Email: email,
        Valido: valido,
        Empresa_ID: empresaId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Empleado actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    empleadoService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Empleado eliminado exitosamente."
      });
    });
  }
};
