const empleadoRolService = require("../services/empleadoRol");

module.exports = {
  all(req, res) {
    empleadoRolService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    empleadoRolService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const {
      activo,
      valido,
      empleadoId,
      rolId,
      perfilId
    } = req.body;

    empleadoRolService.create(
      {
        Activo: activo,
        Valido: valido,
        Empleado_ID: empleadoId,
        Rol_ID: rolId,
        Perfil_ID: perfilId
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Empleado rol creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const {
      activo,
      valido,
      empleadoId,
      rolId,
      perfilId
    } = req.body;
    const { id } = req.params;

    empleadoRolService.update(
      {
        Activo: activo,
        Valido: valido,
        Empleado_ID: empleadoId,
        Rol_ID: rolId,
        Perfil_ID: perfilId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Empleado rol actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    empleadoRolService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Empleado rol eliminado exitosamente."
      });
    });
  }
};
