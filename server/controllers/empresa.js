const empresaService = require("../services/empresa");

module.exports = {
  all(req, res) {
    empresaService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    empresaService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const {
      nombre,
      razonComercial,
      rut,
      telefonoUno,
      telefonoDos,
      direccionUno,
      direccionDos,
      email
    } = req.body;

    empresaService.create(
      {
        Nombre: nombre,
        RazonComercial: razonComercial,
        RUT: rut,
        Telefono1: telefonoUno,
        Telefono2: telefonoDos,
        Direccion1: direccionUno,
        Direccion2: direccionDos,
        Email: email
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Empresa creada exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const {
      nombre,
      razonComercial,
      rut,
      telefonoUno,
      telefonoDos,
      direccionUno,
      direccionDos,
      email
    } = req.body;
    const { id } = req.params;

    empresaService.update(
      {
        Nombre: nombre,
        RazonComercial: razonComercial,
        RUT: rut,
        Telefono1: telefonoUno,
        Telefono2: telefonoDos,
        Direccion1: direccionUno,
        Direccion2: direccionDos,
        Email: email
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Empresa actualizada exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    empresaService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Empresa eliminada exitosamente."
      });
    });
  }
};
