const indicadorService = require("../services/indicador");

module.exports = {
  all(req, res) {
    indicadorService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    indicadorService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  getByObjetivo(req, res) {
    const { objetivoId } = req.params;

    indicadorService.getByObjetivo(objetivoId, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[1]);
    });
  },

  create(req, res) {
    const { codigo, descripcion, objetivoId } = req.body;

    if (descripcion && descripcion !== "" && objetivoId && objetivoId !== "") {
      console.log(descripcion, objetivoId);

      indicadorService.createIndicadorRelacion(objetivoId, (error, result) => {
        if (!error) {
          indicadorService.create(
            {
              Codigo: codigo || "",
              Descripcion: descripcion,
              IndicadorRelacion_ID: result.insertId
            },
            (error, result) => {
              if (error) throw error;

              console.log(result)

              return res.status(201).json({
                code: 201,
                message: "Indicador creado exitosamente.",
                indicadorId: result.insertId
              });
            }
          );
        }
      });
    }
  },

  update(req, res) {
    const { codigo, descripcion, indicadorRelacionId } = req.body;
    const { id } = req.params;

    indicadorService.update(
      {
        Codigo: codigo,
        Descripcion: descripcion,
        IndicadorRelacion_ID: indicadorRelacionId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Indicador actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    indicadorService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Indicador eliminado exitosamente."
      });
    });
  }
};
