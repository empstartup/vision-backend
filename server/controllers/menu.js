const menuService = require("../services/menu");

module.exports = {
  all(req, res) {
    menuService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  }
};
