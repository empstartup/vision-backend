const nivelService = require("../services/nivel");

module.exports = {
  all(req, res) {
    nivelService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    nivelService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const {
      nombre,
      codigo,
      descripcion,
      estado,
      nivelTipoEnsenanzaId
    } = req.body;

    nivelService.create(
      {
        Nombre: nombre,
        Codigo: codigo,
        Descripcion: descripcion,
        Estado: estado,
        NivelTipoEnsenanza_ID: nivelTipoEnsenanzaId
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Nivel tipo enseñanza creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const {
      nombre,
      codigo,
      descripcion,
      estado,
      nivelTipoEnsenanzaId
    } = req.body;
    const { id } = req.params;

    nivelService.update(
      {
        Nombre: nombre,
        Codigo: codigo,
        Descripcion: descripcion,
        Estado: estado,
        NivelTipoEnsenanza_ID: nivelTipoEnsenanzaId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Nivel tipo enseñanza actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    nivelService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Nivel tipo enseñanza eliminado exitosamente."
      });
    });
  }
};
