const nivelAsignaturaService = require("../services/nivelAsignatura");
const initial = require("lodash/initial");

module.exports = {
  all(req, res) {
    nivelAsignaturaService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  generate(req, res) {
    const { menuId, tipoEnsenanzaId } = req.params;
    const { empleadoId } = req.user;

    nivelAsignaturaService.get(
      empleadoId,
      tipoEnsenanzaId,
      menuId,
      (error, result) => {
        if (error) throw error;

        return res.status(200).json(initial(result));
      }
    );
  },

  get(req, res) {
    const { id } = req.params;

    nivelAsignaturaService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const { codigo, descripcion, estado, nivelId, asignaturaId } = req.body;

    nivelAsignaturaService.create(
      {
        Codigo: codigo,
        Descripcion: descripcion,
        Estado: estado,
        Nivel_ID: nivelId,
        Asignatura_ID: asignaturaId
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Nivel asignatura creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const { codigo, descripcion, estado, nivelId, asignaturaId } = req.body;
    const { id } = req.params;

    nivelAsignaturaService.update(
      {
        Codigo: codigo,
        Descripcion: descripcion,
        Estado: estado,
        Nivel_ID: nivelId,
        Asignatura_ID: asignaturaId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Nivel asignatura actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    nivelAsignaturaService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Nivel asignatura eliminado exitosamente."
      });
    });
  }
};
