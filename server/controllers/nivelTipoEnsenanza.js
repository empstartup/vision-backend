const nivelTipoEnsenanzaService = require("../services/nivelTipoEnsenanza");

module.exports = {
  all(req, res) {
    nivelTipoEnsenanzaService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    nivelTipoEnsenanzaService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const {
      nombre,
      codigo,
      descripcion,
      estado,
      tipoEnsenanzaId
    } = req.body;

    nivelTipoEnsenanzaService.create(
      {
        Nombre: nombre,
        Codigo: codigo,
        Descripcion: descripcion,
        Estado: estado,
        TipoEnsenanza_ID: tipoEnsenanzaId
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Tipo enseñanza creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const {
      nombre,
      codigo,
      descripcion,
      estado,
      tipoEnsenanzaId
    } = req.body;
    const { id } = req.params;

    nivelTipoEnsenanzaService.update(
      {
        Nombre: nombre,
        Codigo: codigo,
        Descripcion: descripcion,
        Estado: estado,
        TipoEnsenanza_ID: tipoEnsenanzaId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Tipo enseñanza actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    nivelTipoEnsenanzaService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Tipo enseñanza eliminado exitosamente."
      });
    });
  }
};
