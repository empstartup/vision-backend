const objetivoService = require("../services/objetivo");

module.exports = {
  all(req, res) {
    objetivoService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    objetivoService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  getByNivelAsignatura(req, res) {
    const { nivelAsignaturaId } = req.params;

    objetivoService.getByNivelAsignatura(nivelAsignaturaId, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  getByCompetencia(req, res) {
    const { competenciaId } = req.params;

    objetivoService.getByCompetencia(competenciaId, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const { codigo, nombre, descripcion, estado } = req.body;

    objetivoService.create(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Estado: estado
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Objetivo creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const { codigo, nombre, descripcion, estado } = req.body;
    const { id } = req.params;

    objetivoService.update(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Estado: estado
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Objetivo actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    objetivoService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Objetivo eliminado exitosamente."
      });
    });
  }
};
