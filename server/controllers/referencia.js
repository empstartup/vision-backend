const referenciaService = require("../services/referencia");

module.exports = {
  all(req, res) {
    referenciaService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    referenciaService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const {
      codigo,
      texto,
      orden,
      tipoReferenciaId,
      colorReferenciaId,
      referenciaRelacionId
    } = req.body;

    referenciaService.create(
      {
        Codigo: codigo,
        Texto: texto,
        Orden: orden,
        TipoReferencia_ID: tipoReferenciaId,
        ColorReferencia_ID: colorReferenciaId,
        ReferenciaRelacion_ID: referenciaRelacionId
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Referencia creada exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const {
      codigo,
      texto,
      orden,
      tipoReferenciaId,
      colorReferenciaId,
      referenciaRelacionId
    } = req.body;
    const { id } = req.params;

    referenciaService.update(
      {
        Codigo: codigo,
        Texto: texto,
        Orden: orden,
        TipoReferencia_ID: tipoReferenciaId,
        ColorReferencia_ID: colorReferenciaId,
        ReferenciaRelacion_ID: referenciaRelacionId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Referencia actualizada exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    referenciaService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Referencia eliminada exitosamente."
      });
    });
  }
};
