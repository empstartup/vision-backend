const rolService = require("../services/rol");

module.exports = {
  all(req, res) {
    rolService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    rolService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const { codigo, nombre, descripcion, sigla, valido } = req.body;

    rolService.create(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Sigla: sigla,
        Valido: valido
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Rol creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const { codigo, nombre, descripcion, sigla, valido } = req.body;
    const { id } = req.params;

    rolService.update(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Sigla: sigla,
        Valido: valido
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Rol actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    rolService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Rol eliminado exitosamente."
      });
    });
  }
};
