const saberService = require("../services/saber");

module.exports = {
  all(req, res) {
    saberService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    saberService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  getLista(req, res) {
    const { competenciaId, saberId } = req.params;

    saberService.getLista(competenciaId, saberId, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  getElemento(req, res) {
    const { competenciaId, saberId, elementoId } = req.params;

    saberService.getElemento(
      competenciaId,
      saberId,
      elementoId,
      (error, result) => {
        if (error) throw error;

        return res.status(200).json(result[0]);
      }
    );
  },

  create(req, res) {
    const { codigo, nombre, descripcion, estado } = req.body;

    saberService.create(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Estado: estado
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Saber creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const { codigo, nombre, descripcion, estado } = req.body;
    const { id } = req.params;

    saberService.update(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Estado: estado
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Saber actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    saberService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Saber eliminado exitosamente."
      });
    });
  }
};
