const secuenciaService = require("../services/secuencia");

module.exports = {
  all(req, res) {
    secuenciaService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    secuenciaService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  getByNivelAsignatura(req, res) {
    const { nivelAsignaturaId } = req.params;

    secuenciaService.getByNivelAsignatura(
      nivelAsignaturaId,
      (error, result) => {
        if (error) throw error;

        return res.status(200).json(result);
      }
    );
  },

  create(req, res) {
    const {
      codigo,
      nombre,
      descripcion,
      horas,
      fechaDesde,
      fechaHasta,
      estado,
      competenciaGenericaId,
      nivelAsignaturaId
    } = req.body;

    secuenciaService.create(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Horas: horas,
        FechaDesde: fechaDesde,
        FechaHasta: fechaHasta,
        Estado: estado,
        CompetenciaGenerica_ID: competenciaGenericaId,
        NivelAsignatura_ID: nivelAsignaturaId
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Secuencia creada exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const {
      codigo,
      nombre,
      descripcion,
      horas,
      fechaDesde,
      fechaHasta,
      estado,
      competenciaGenericaId,
      nivelAsignaturaId
    } = req.body;
    const { id } = req.params;

    secuenciaService.update(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Horas: horas,
        FechaDesde: fechaDesde,
        FechaHasta: fechaHasta,
        Estado: estado,
        CompetenciaGenerica_ID: competenciaGenericaId,
        NivelAsignatura_ID: nivelAsignaturaId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Secuencia actualizada exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    secuenciaService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Secuencia eliminada exitosamente."
      });
    });
  }
};
