const seguimientoCompetenciasService = require("../services/seguimientoCompetencias");

module.exports = {
  get(req, res) {
    const { subEjeId } = req.params;

    seguimientoCompetenciasService.get(subEjeId, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  }
};
