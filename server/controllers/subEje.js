const subEjeService = require("../services/subEje");

module.exports = {
  all(req, res) {
    subEjeService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    subEjeService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const {
      nombre,
      codigo,
      descripcion,
      estado,
      ejeId
    } = req.body;

    subEjeService.create(
      {
        Nombre: nombre,
        Codigo: codigo,
        Descripcion: descripcion,
        Estado: estado,
        Eje_ID: ejeId
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Sub eje creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const {
      nombre,
      codigo,
      descripcion,
      estado,
      ejeId
    } = req.body;
    const { id } = req.params;

    subEjeService.update(
      {
        Nombre: nombre,
        Codigo: codigo,
        Descripcion: descripcion,
        Estado: estado,
        Eje_ID: ejeId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Sub eje actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    subEjeService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Sub eje eliminado exitosamente."
      });
    });
  }
};
