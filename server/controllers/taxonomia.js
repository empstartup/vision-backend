const taxonomiaService = require("../services/taxonomia");

module.exports = {
  all(req, res) {
    taxonomiaService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    taxonomiaService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const { codigo, nombre, descripcion, estado } = req.body;

    taxonomiaService.create(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Estado: estado
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Taxonomia creada exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const { codigo, nombre, descripcion, estado } = req.body;
    const { id } = req.params;

    taxonomiaService.update(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion,
        Estado: estado
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Taxonomia actualizada exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    taxonomiaService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Taxonomia eliminada exitosamente."
      });
    });
  }
};
