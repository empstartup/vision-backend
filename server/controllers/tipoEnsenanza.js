const tipoEnsenanzaController = require("../services/tipoEnsenanza");

module.exports = {
  all(req, res) {
    tipoEnsenanzaController.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  get(req, res) {
    const { id } = req.params;

    tipoEnsenanzaController.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const { codigo, nombre, descripcion } = req.body;

    tipoEnsenanzaController.create(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Tipo enseñanza creada exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const { codigo, nombre, descripcion } = req.body;
    const { id } = req.params;

    tipoEnsenanzaController.update(
      {
        Codigo: codigo,
        Nombre: nombre,
        Descripcion: descripcion
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Tipo enseñanza actualizada exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    tipoEnsenanzaController.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Tipo enseñanza eliminada exitosamente."
      });
    });
  }
};
