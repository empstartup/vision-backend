const tipoReferenciaController = require("../services/tipoReferencia");

module.exports = {
  all(req, res) {
    tipoReferenciaController.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    tipoReferenciaController.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const { descripcion } = req.body;

    tipoReferenciaController.create(
      {
        Descripcion: descripcion
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Tipo referencia creada exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const { descripcion } = req.body;
    const { id } = req.params;

    tipoReferenciaController.update(
      {
        Descripcion: descripcion
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Tipo referencia actualizada exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    tipoReferenciaController.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Tipo referencia eliminada exitosamente."
      });
    });
  }
};
