const usuarioService = require("../services/usuario");

module.exports = {
  all(req, res) {
    usuarioService.getAll((error, result) => {
      if (error) throw error;

      return res.status(200).json(result);
    });
  },

  get(req, res) {
    const { id } = req.params;

    usuarioService.getById(id, (error, result) => {
      if (error) throw error;

      return res.status(200).json(result[0]);
    });
  },

  create(req, res) {
    const {
      usuario,
      clave,
      estado,
      ultimoAcceso,
      empleadoId
    } = req.body;

    usuarioService.create(
      {
        Usuario: usuario,
        Clave: clave,
        Estado: estado,
        UltimoAcceso: ultimoAcceso,
        Empleado_ID: empleadoId
      },
      (error, result) => {
        if (error) throw error;

        return res.status(201).json({
          code: 201,
          message: "Usuario creado exitosamente.",
          asignaturaId: result.insertId
        });
      }
    );
  },

  update(req, res) {
    const {
      usuario,
      clave,
      estado,
      ultimoAcceso,
      empleadoId
    } = req.body;
    const { id } = req.params;

    usuarioService.update(
      {
        Usuario: usuario,
        Clave: clave,
        Estado: estado,
        UltimoAcceso: ultimoAcceso,
        Empleado_ID: empleadoId
      },
      id,
      error => {
        if (error) throw error;

        return res.status(204).json({
          code: 204,
          message: "Usuario actualizado exitosamente."
        });
      }
    );
  },

  delete(req, res) {
    const { id } = req.params;

    usuarioService.delete(id, error => {
      if (error) throw error;

      return res.status(204).json({
        code: 204,
        message: "Usuario eliminado exitosamente."
      });
    });
  }
};
