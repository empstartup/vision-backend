const jwt = require("jsonwebtoken");
const _ = require("lodash");
const bcrypt = require("bcrypt");
const jwtSecret = process.env.JWT_SECRET || "12345";

const pw = bcrypt.hashSync("12345", 5);
console.log("PWWWW", pw);

module.exports = {
  verify: token => {
    return new Promise(function(resolve, reject) {
      jwt.verify(token, jwtSecret, (error, decodedToken) => {
        if (error || !decodedToken) {
          return reject(error);
        }

        resolve(decodedToken);
      });
    });
  },
  create: options => {
    if (typeof options !== "object") {
      options = {};
    }

    if (!options.maxAge || typeof options.maxAge !== "number") {
      options.maxAge = 3600;
    }

    options.sessionData = _.reduce(
      options.sessionData || {},
      (array, val, key) => {
        if (typeof val !== "function" && key !== "Clave") {
          array[key] = val;
        }
        return array;
      },
      {}
    );

    return jwt.sign(
      {
        data: options.sessionData
      },
      jwtSecret,
      {
        expiresIn: options.maxAge,
        algorithm: "HS256"
      }
    );
  },
  comparePassword: (user, key, password) => {
    return new Promise((resolve, reject) => {
      return bcrypt.compare(password, user[key], (err, same) => {
        console.log(password, user[key]);
        console.log("same", same);
        if (err || !same) {
          reject(false);
          return;
        }

        resolve(true);
      });
    });
  }
};
