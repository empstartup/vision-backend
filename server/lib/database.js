const mysql = require("mysql");
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "araucania",
  database: "vision",
  debug: true
});

connection.connect();

module.exports = connection;
