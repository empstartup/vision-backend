const { verify } = require("./auth");

module.exports = {
  jwtMiddleware: (req, res, next) => {
    const token = req.headers.authorization;

    if (!token) {
      return res.status(401).json({
        message: "No tiene acceso a esta ruta. Por favor, cree un nuevo token."
      });
    }

    verify(token)
      .then(decodedToken => {
        console.log(decodedToken);
        req.user = decodedToken.data;
        next();
      })
      .catch(() => {
        res.clearCookie("accessToken");
        res.status(401).json({
          message: "El token recibido no es válido."
        });
      });
  },

  checkParam: params => {
    return (req, res, next) => {
      if (!req.body) {
        res.status(400);
        next(new Error("No Body."));
      } else {
        for (let param of params) {
          // Nested Object Inpection
          let periodIndex = param.indexOf(".");
          if (periodIndex === -1) {
            if (req.body[param] === undefined) {
              return res.status(400).json({
                code: 400,
                message: `El parámetro '${param}' es obligatorio.`
              });
            }
          } else {
            // Nested Object
            let objects = param.split(".");
            if (objects.length > 2) {
              next(new Error("Nested Object Checking Limited to 2"));
            } else {
              if (
                req.body[objects[0]] === undefined ||
                req.body[objects[0]][objects[1]] === undefined
              ) {
                res.status(400);
                next(new Error(`El parámetro '${param}' es obligatorio.`));
                return;
              }
            }
          }
        }
        next();
      }
    };
  },

  isAuthenticated: (req, res, next) => {
    if (req.headers.authorization === "") {
      console.log(req.headers.authorization);
      return res.status(401).json({
        code: 401,
        message: "Acceso denegado"
      });
    }

    next();
  }
};
