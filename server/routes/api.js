const authorizationRoutes = require("./auth");
const asignaturaRoutes = require("./asignatura");
const colorReferenciaRoutes = require("./colorReferencia");
const competenciaGenericaRoutes = require("./competenciaGenerica");
const empresaRoutes = require("./empresa");
const objetivoRoutes = require("./objetivo");
const perfilRoutes = require("./perfil");
const rolRoutes = require("./rol");
const saberRoutes = require("./saber");
const taxonomiaRoutes = require("./taxonomia");
const tipoEnsenanzaRoutes = require("./tipoEnsenanza");
const tipoReferenciaRoutes = require("./tipoReferencia");
const ejeRoutes = require("./eje");
const empleadoRoutes = require("./empleado");
const nivelTipoEnsenanzaRoutes = require("./nivelTipoEnsenanza");
const subEjeRoutes = require("./subEje");
const usuarioRoutes = require("./usuario");
const nivelRoutes = require("./nivel");
const nivelAsignaturaRoutes = require("./nivelAsignatura");
const empleadoRolRoutes = require("./empleadoRol");
const referenciaRoutes = require("./referencia");
const archivoRoutes = require("./archivo");
const indicadorRoutes = require("./indicador");
const menuRoutes = require("./menu");
const secuenciaRoutes = require("./secuencia");
const seguimientoCompetenciasRoutes = require("./seguimientoCompetencias");
const competenciasRoutes = require("./competencia");

module.exports = {
  authorizationRoutes,
  asignaturaRoutes,
  colorReferenciaRoutes,
  competenciaGenericaRoutes,
  empresaRoutes,
  objetivoRoutes,
  perfilRoutes,
  rolRoutes,
  saberRoutes,
  taxonomiaRoutes,
  tipoEnsenanzaRoutes,
  tipoReferenciaRoutes,
  ejeRoutes,
  empleadoRoutes,
  subEjeRoutes,
  usuarioRoutes,
  nivelTipoEnsenanzaRoutes,
  nivelRoutes,
  nivelAsignaturaRoutes,
  empleadoRolRoutes,
  referenciaRoutes,
  archivoRoutes,
  indicadorRoutes,
  menuRoutes,
  secuenciaRoutes,
  seguimientoCompetenciasRoutes,
  competenciasRoutes
};
