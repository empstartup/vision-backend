const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const archivoController = require("../controllers/archivo");

router.get("/", [isAuthenticated, jwtMiddleware], archivoController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], archivoController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["nombre", "extension"])
  ],
  archivoController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["nombre", "extension"])
  ],
  archivoController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  archivoController.delete
);

module.exports = router;
