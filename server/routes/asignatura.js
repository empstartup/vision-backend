const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const asignaturaController = require("../controllers/asignatura");

router.get("/", [isAuthenticated, jwtMiddleware], asignaturaController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], asignaturaController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "estado"])
  ],
  asignaturaController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "estado"])
  ],
  asignaturaController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  asignaturaController.delete
);

module.exports = router;
