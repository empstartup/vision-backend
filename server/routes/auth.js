const express = require("express");
const router = express.Router();
const { checkParam } = require("../lib/middlewares");
const { create, comparePassword } = require("../lib/auth");

const { authenticate } = require("../services/usuario");

function hasInvalidCredentials(res) {
  return res.status(401).json({
    code: 401,
    message: "Las incredenciales ingresadas no son correctas."
  });
}

router.post("*", checkParam(["username", "password"]));

router.post("/", (req, res) => {
  const { username, password } = req.body;

  authenticate(username, (error, results) => {
    if (results.length === 0 || error) {
      return hasInvalidCredentials(res);
    }

    const user = results[0];

    comparePassword(user, "Clave", password)
      .then(isValid => {
        console.log(" RESULTS *************************** ", isValid);
        if (!isValid) return hasInvalidCredentials(res);

        const token = create({
          sessionData: {
            id: user.ID,
            usuario: user.Usuario,
            empleadoId: user.Empleado_ID
          },
          maxAge: 3600
        });

        res.cookie("accessToken", token);
        res.status(200).json({
          code: 200,
          token
        });
      })
      .catch(() => {
        console.log(" *********** HERE ************ ");
        return hasInvalidCredentials(res);
      });
  });
});

router.post("/logout", (req, res) => {
  res.clearCookie("accessToken");

  return res.status(200).json({
    code: 200,
    message: "Has cerrado sesión correctamente."
  });
});

module.exports = router;
