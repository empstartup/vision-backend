const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const colorReferenciaController = require("../controllers/colorReferencia");

router.get(
  "/",
  [isAuthenticated, jwtMiddleware],
  colorReferenciaController.all
);

router.get(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  colorReferenciaController.get
);

router.post(
  "/",
  [isAuthenticated, jwtMiddleware, checkParam(["codigo", "nombre"])],
  colorReferenciaController.create
);

router.put(
  "/:id",
  [isAuthenticated, jwtMiddleware, checkParam(["codigo", "nombre"])],
  colorReferenciaController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  colorReferenciaController.delete
);

module.exports = router;
