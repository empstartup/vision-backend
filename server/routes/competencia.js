const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const competenciaController = require("../controllers/competencia");

router.get(
  "/",
  [isAuthenticated, jwtMiddleware],
  competenciaController.all
);

router.get(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  competenciaController.get
);

router.get(
  "/lista/:nivelAsignaturaId",
  [isAuthenticated, jwtMiddleware],
  competenciaController.getByNivelAsignatura
);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion"])
  ],
  competenciaController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion"])
  ],
  competenciaController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  competenciaController.delete
);

module.exports = router;
