const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const competenciaGenericaController = require("../controllers/competenciaGenerica");

router.get(
  "/",
  [isAuthenticated, jwtMiddleware],
  competenciaGenericaController.all
);

router.get(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  competenciaGenericaController.get
);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion"])
  ],
  competenciaGenericaController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion"])
  ],
  competenciaGenericaController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  competenciaGenericaController.delete
);

module.exports = router;
