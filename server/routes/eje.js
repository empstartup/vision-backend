const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const ejeController = require("../controllers/eje");

router.get("/", [isAuthenticated, jwtMiddleware], ejeController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], ejeController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "estado", "asignaturaId"])
  ],
  ejeController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "estado", "asignaturaId"])
  ],
  ejeController.update
);

router.delete("/:id", [isAuthenticated, jwtMiddleware], ejeController.delete);

module.exports = router;
