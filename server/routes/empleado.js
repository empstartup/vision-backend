const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const empleadoController = require("../controllers/empleado");

router.get("/", [isAuthenticated, jwtMiddleware], empleadoController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], empleadoController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "nombre",
      "apellidoPaterno",
      "apellidoMaterno",
      "edad",
      "rut",
      "sexo",
      "direccion",
      "telefonoUno",
      "telefonoDos",
      "email",
      "valido",
      "empresaId"
    ])
  ],
  empleadoController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "nombre",
      "apellidoPaterno",
      "apellidoMaterno",
      "edad",
      "rut",
      "sexo",
      "direccion",
      "telefonoUno",
      "telefonoDos",
      "email",
      "valido",
      "empresaId"
    ])
  ],
  empleadoController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  empleadoController.delete
);

module.exports = router;
