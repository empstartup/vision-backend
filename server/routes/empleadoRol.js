const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const empleadoRolController = require("../controllers/empleadoRol");

router.get("/", [isAuthenticated, jwtMiddleware], empleadoRolController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], empleadoRolController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "activo",
      "valido",
      "empleadoId",
      "rolId",
      "perfilId"
    ])
  ],
  empleadoRolController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "activo",
      "valido",
      "empleadoId",
      "rolId",
      "perfilId"
    ])
  ],
  empleadoRolController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  empleadoRolController.delete
);

module.exports = router;
