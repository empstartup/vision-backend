const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const empresaController = require("../controllers/empresa");

router.get("/", [isAuthenticated, jwtMiddleware], empresaController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], empresaController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "nombre",
      "razonComercial",
      "rut",
      "telefonoUno",
      "telefonoDos",
      "direccionUno",
      "direccionDos"
    ])
  ],
  empresaController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "nombre",
      "razonComercial",
      "rut",
      "telefonoUno",
      "telefonoDos",
      "direccionUno",
      "direccionDos"
    ])
  ],
  empresaController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  empresaController.delete
);

module.exports = router;
