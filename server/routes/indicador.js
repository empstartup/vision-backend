const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const indicadorController = require("../controllers/indicador");

router.get("/", [isAuthenticated, jwtMiddleware], indicadorController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], indicadorController.get);

router.get(
  "/objetivo/:objetivoId",
  [isAuthenticated, jwtMiddleware],
  indicadorController.getByObjetivo
);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["descripcion", "objetivoId"])
  ],
  indicadorController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["descripcion"])
  ],
  indicadorController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  indicadorController.delete
);

module.exports = router;
