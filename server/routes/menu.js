const express = require("express");
const router = express.Router();
const {
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const menusController = require("../controllers/menu");

router.get("/", [isAuthenticated, jwtMiddleware], menusController.all);

module.exports = router;
