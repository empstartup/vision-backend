const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const nivelController = require("../controllers/nivel");

router.get("/", [isAuthenticated, jwtMiddleware], nivelController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], nivelController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "nombre",
      "codigo",
      "descripcion",
      "estado",
      "nivelTipoEnsenanzaId"
    ])
  ],
  nivelController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "nombre",
      "codigo",
      "descripcion",
      "estado",
      "nivelTipoEnsenanzaId"
    ])
  ],
  nivelController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  nivelController.delete
);

module.exports = router;
