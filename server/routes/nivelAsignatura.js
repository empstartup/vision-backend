const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const nivelAsignaturaController = require("../controllers/nivelAsignatura");

router.get(
  "/",
  [isAuthenticated, jwtMiddleware],
  nivelAsignaturaController.all
);

router.get(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  nivelAsignaturaController.get
);

router.get(
  "/:menuId/:tipoEnsenanzaId",
  [isAuthenticated, jwtMiddleware],
  nivelAsignaturaController.generate
);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "descripcion", "estado", "nivelId", "asignaturaId"])
  ],
  nivelAsignaturaController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "descripcion", "estado", "nivelId", "asignaturaId"])
  ],
  nivelAsignaturaController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  nivelAsignaturaController.delete
);

module.exports = router;
