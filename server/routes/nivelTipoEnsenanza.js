const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const nivelTipoEnsenanzaController = require("../controllers/nivelTipoEnsenanza");

router.get("/", [isAuthenticated, jwtMiddleware], nivelTipoEnsenanzaController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], nivelTipoEnsenanzaController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "nombre",
      "codigo",
      "descripcion",
      "estado",
      "tipoEnsenanzaId"
    ])
  ],
  nivelTipoEnsenanzaController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "nombre",
      "codigo",
      "descripcion",
      "estado",
      "tipoEnsenanzaId"
    ])
  ],
  nivelTipoEnsenanzaController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  nivelTipoEnsenanzaController.delete
);

module.exports = router;
