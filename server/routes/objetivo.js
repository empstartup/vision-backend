const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const objetivoController = require("../controllers/objetivo");

router.get("/", [isAuthenticated, jwtMiddleware], objetivoController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], objetivoController.get);

router.get(
  "/lista/:nivelAsignaturaId",
  [isAuthenticated, jwtMiddleware],
  objetivoController.getByNivelAsignatura
);

router.get(
  "/lista/competencias/:competenciaId",
  [isAuthenticated, jwtMiddleware],
  objetivoController.getByCompetencia
);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "estado"])
  ],
  objetivoController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "estado"])
  ],
  objetivoController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  objetivoController.delete
);

module.exports = router;
