const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const perfilController = require("../controllers/perfil");

router.get("/", [isAuthenticated, jwtMiddleware], perfilController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], perfilController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "valido"])
  ],
  perfilController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "valido"])
  ],
  perfilController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  perfilController.delete
);

module.exports = router;
