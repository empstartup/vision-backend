const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const referenciaController = require("../controllers/referencia");

router.get("/", [isAuthenticated, jwtMiddleware], referenciaController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], referenciaController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "texto", "orden", "tipoReferenciaId", "colorReferenciaId", "referenciaRelacionId"])
  ],
  referenciaController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "texto", "orden", "tipoReferenciaId", "colorReferenciaId", "referenciaRelacionId"])
  ],
  referenciaController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  referenciaController.delete
);

module.exports = router;
