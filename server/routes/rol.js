const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const rolController = require("../controllers/rol");

router.get("/", [isAuthenticated, jwtMiddleware], rolController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], rolController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "valido", "sigla"])
  ],
  rolController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "valido", "sigla"])
  ],
  rolController.update
);

router.delete("/:id", [isAuthenticated, jwtMiddleware], rolController.delete);

module.exports = router;
