const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const saberController = require("../controllers/saber");

router.get("/", [isAuthenticated, jwtMiddleware], saberController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], saberController.get);

router.get(
  "/lista/:competenciaId/:saberId",
  [isAuthenticated, jwtMiddleware],
  saberController.getLista
);

router.get(
  "/lista/:competenciaId/:saberId/:elementoId",
  [isAuthenticated, jwtMiddleware],
  saberController.getElemento
);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "estado"])
  ],
  saberController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "estado"])
  ],
  saberController.update
);

router.delete("/:id", [isAuthenticated, jwtMiddleware], saberController.delete);

module.exports = router;
