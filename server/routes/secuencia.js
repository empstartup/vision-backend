const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const secuenciaController = require("../controllers/secuencia");

router.get(
  "/",
  [isAuthenticated, jwtMiddleware],
  secuenciaController.all
);

router.get(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  secuenciaController.get
);

router.get(
  "/lista/:nivelAsignaturaId",
  [isAuthenticated, jwtMiddleware],
  secuenciaController.getByNivelAsignatura
);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion"])
  ],
  secuenciaController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion"])
  ],
  secuenciaController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  secuenciaController.delete
);

module.exports = router;
