const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const seguimientoCompetenciasController = require("../controllers/seguimientoCompetencias");

router.get(
  "/:subEjeId",
  [isAuthenticated, jwtMiddleware],
  seguimientoCompetenciasController.get
);

module.exports = router;
