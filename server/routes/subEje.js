const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const subEjeController = require("../controllers/subEje");

router.get("/", [isAuthenticated, jwtMiddleware], subEjeController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], subEjeController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "nombre",
      "codigo",
      "descripcion",
      "estado",
      "ejeId"
    ])
  ],
  subEjeController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "nombre",
      "codigo",
      "descripcion",
      "estado",
      "ejeId"
    ])
  ],
  subEjeController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  subEjeController.delete
);

module.exports = router;
