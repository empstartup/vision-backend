const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const taxonomiaController = require("../controllers/taxonomia");

router.get("/", [isAuthenticated, jwtMiddleware], taxonomiaController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], taxonomiaController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "estado"])
  ],
  taxonomiaController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion", "estado"])
  ],
  taxonomiaController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  taxonomiaController.delete
);

module.exports = router;
