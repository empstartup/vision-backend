const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const tipoEnsenanzaController = require("../controllers/tipoEnsenanza");

router.get("/", [isAuthenticated, jwtMiddleware], tipoEnsenanzaController.all);

router.get(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  tipoEnsenanzaController.get
);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion"])
  ],
  tipoEnsenanzaController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam(["codigo", "nombre", "descripcion"])
  ],
  tipoEnsenanzaController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  tipoEnsenanzaController.delete
);

module.exports = router;
