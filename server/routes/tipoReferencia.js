const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const tipoReferenciaController = require("../controllers/tipoReferencia");

router.get("/", [isAuthenticated, jwtMiddleware], tipoReferenciaController.all);

router.get(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  tipoReferenciaController.get
);

router.post(
  "/",
  [isAuthenticated, jwtMiddleware, checkParam(["descripcion"])],
  tipoReferenciaController.create
);

router.put(
  "/:id",
  [isAuthenticated, jwtMiddleware, checkParam(["descripcion"])],
  tipoReferenciaController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  tipoReferenciaController.delete
);

module.exports = router;
