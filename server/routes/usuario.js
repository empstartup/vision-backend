const express = require("express");
const router = express.Router();
const {
  checkParam,
  isAuthenticated,
  jwtMiddleware
} = require("../lib/middlewares");
const usuarioController = require("../controllers/usuario");

router.get("/", [isAuthenticated, jwtMiddleware], usuarioController.all);

router.get("/:id", [isAuthenticated, jwtMiddleware], usuarioController.get);

router.post(
  "/",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "usuario",
      "clave",
      "estado",
      "ultimoAcceso",
      "empleadoId"
    ])
  ],
  usuarioController.create
);

router.put(
  "/:id",
  [
    isAuthenticated,
    jwtMiddleware,
    checkParam([
      "usuario",
      "clave",
      "estado",
      "ultimoAcceso",
      "empleadoId"
    ])
  ],
  usuarioController.update
);

router.delete(
  "/:id",
  [isAuthenticated, jwtMiddleware],
  usuarioController.delete
);

module.exports = router;
