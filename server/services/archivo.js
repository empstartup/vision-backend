const GeneralService = require("./generalService");
const service = new GeneralService("archivos");

module.exports = {
  getAll(callback) {
    return service.getAll(callback);
  },

  getById(id, callback) {
    return service.getById(id, callback);
  },

  create(asignatura, callback) {
    return service.create(asignatura, callback);
  },

  update(asignatura, id, callback) {
    return service.update(asignatura, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
