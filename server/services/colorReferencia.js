const GeneralService = require("./generalService");
const service = new GeneralService("colorreferencia");

module.exports = {
  getAll(callback) {
    return service.getAll(callback);
  },

  getById(id, callback) {
    return service.getById(id, callback);
  },

  getByCode(code, callback) {
    return service.getByCode(code, callback);
  },

  create(colorReferencia, callback) {
    return service.create(colorReferencia, callback);
  },

  update(colorReferencia, id, callback) {
    return service.update(colorReferencia, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
