const GeneralService = require("./generalService");
const service = new GeneralService("competencia");
const db = require("../lib/database");

module.exports = {
  getAll(callback) {
    return service.getAll(callback);
  },

  getById(id, callback) {
    if (!callback) throw Error("Callback is necessary.");

    return db.query(
      {
        sql: `call prcCompetencia(${id})`
      },
      callback
    );
  },

  getByNivelAsignatura(nivelAsignaturaId, callback) {
    if (!callback) throw Error("Callback is necessary.");

    return db.query(
      {
        sql: `call prcCompetenciaLista(${nivelAsignaturaId}, 0, 1000)`
      },
      callback
    );
  },

  getByCode(code, callback) {
    return service.getByCode(code, callback);
  },

  create(competencia, callback) {
    return service.create(competencia, callback);
  },

  update(competencia, id, callback) {
    return service.update(competencia, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
