const GeneralService = require("./generalService");
const service = new GeneralService("competenciagenerica");

module.exports = {
  getAll(callback) {
    return service.getAll(callback);
  },

  getById(id, callback) {
    return service.getById(id, callback);
  },

  getByCode(code, callback) {
    return service.getByCode(code, callback);
  },

  create(competenciaGenerica, callback) {
    return service.create(competenciaGenerica, callback);
  },

  update(competenciaGenerica, id, callback) {
    return service.update(competenciaGenerica, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
