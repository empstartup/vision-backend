const GeneralService = require("./generalService");
const service = new GeneralService("empresa");

module.exports = {
  getAll(callback) {
    return service.getAll(callback);
  },

  getById(id, callback) {
    return service.getById(id, callback);
  },

  getByCode(code, callback) {
    return service.getByCode(code, callback);
  },

  create(empresa, callback) {
    return service.create(empresa, callback);
  },

  update(empresa, id, callback) {
    return service.update(empresa, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
