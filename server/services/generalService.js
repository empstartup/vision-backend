const db = require("../lib/database");

class GeneralService {
  constructor(tableName) {
    this.tableName = tableName;
  }

  getAll(callback) {
    if (!callback) throw Error("Callback is necessary.");

    return db.query(`SELECT * FROM ${this.tableName}`, callback);
  }

  getById(id, callback) {
    if (!callback) throw Error("Callback is necessary.");

    return db.query(
      {
        sql: `SELECT * FROM ${this.tableName} WHERE ID = ?`,
        values: id
      },
      callback
    );
  }

  getByCode(code, callback) {
    if (!callback) throw Error("Callback is necessary.");

    return db.query(
      {
        sql: `SELECT * FROM ${this.tableName} WHERE Codigo = ?`,
        values: code
      },
      callback
    );
  }

  create(values, callback) {
    if (!callback) throw Error("Callback is necessary.");

    const now = new Date().toLocaleString();

    return db.query(
      {
        sql: `INSERT INTO ${this.tableName} SET ?`,
        values: {
          ...values,
          FechaCreacion: now
        }
      },
      callback
    );
  }

  update(values, id, callback) {
    if (!callback) throw Error("Callback is necessary.");

    db.query(
      `UPDATE ${this.tableName} SET ? WHERE ID = ?`,
      [values, id],
      callback
    );
  }

  delete(id, callback) {
    if (!callback) throw Error("Callback is necessary.");
    console.log("here");

    db.query(`DELETE FROM ${this.tableName} WHERE ID = ?`, id, callback);
  }
}

module.exports = GeneralService;
