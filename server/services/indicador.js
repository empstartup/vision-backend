const GeneralService = require("./generalService");
const service = new GeneralService("indicador");
const db = require("../lib/database");

module.exports = {
  getAll(callback) {
    return service.getAll(callback);
  },

  getByObjetivo(objetivoId, callback) {
    return db.query(`CALL prcIndicadoresPorObjetivo(${objetivoId})`, callback);
  },

  getById(id, callback) {
    return service.getById(id, callback);
  },

  create(indicador, callback) {
    return service.create(indicador, callback);
  },

  createIndicadorRelacion(objetivoId, callback) {
    const now = new Date().toLocaleString();

    return db.query(
      {
        sql: `INSERT INTO indicadorrelacion SET ?`,
        values: {
          TablaNombre: 'Objetivo',
          TablaID: objetivoId,
          Estado: 'A',
          FechaCreacion: now
        }
      },
      callback
    );
  },

  update(indicador, id, callback) {
    return service.update(indicador, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
