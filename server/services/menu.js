const GeneralService = require("./generalService");
const service = new GeneralService("parametros");
const db = require("../lib/database");

module.exports = {
  getAll(callback) {
    return db.query("CALL prcMenu()", callback);
  },

  getById(id, callback) {},

  create(asignatura, callback) {},

  update(asignatura, id, callback) {},

  delete(id, callback) {}
};
