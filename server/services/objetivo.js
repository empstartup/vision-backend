const GeneralService = require("./generalService");
const service = new GeneralService("objetivo");
const db = require("../lib/database");

module.exports = {
  getByNivelAsignatura(nivelAsignaturaId, callback) {
    return db.query(`CALL prcObjetivosPorEje(${nivelAsignaturaId})`, callback);
  },

  getByCompetencia(competenciaId, callback) {
    return db.query(
      `CALL prcObjetivoLista(${competenciaId}, 0, 1000)`,
      callback
    );
  },

  getAll(callback) {
    return service.getAll(callback);
  },

  getById(id, callback) {
    return service.getById(id, callback);
  },

  getByCode(code, callback) {
    return service.getByCode(code, callback);
  },

  create(objetivo, callback) {
    return service.create(objetivo, callback);
  },

  update(objetivo, id, callback) {
    return service.update(objetivo, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
