const GeneralService = require("./generalService");
const service = new GeneralService("perfil");

module.exports = {
  getAll(callback) {
    return service.getAll(callback);
  },

  getById(id, callback) {
    return service.getById(id, callback);
  },

  getByCode(code, callback) {
    return service.getByCode(code, callback);
  },

  create(perfil, callback) {
    return service.create(perfil, callback);
  },

  update(perfil, id, callback) {
    return service.update(perfil, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
