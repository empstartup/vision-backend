const GeneralService = require("./generalService");
const service = new GeneralService("rol");

module.exports = {
  getAll(callback) {
    return service.getAll(callback);
  },

  getById(id, callback) {
    return service.getById(id, callback);
  },

  getByCode(code, callback) {
    return service.getByCode(code, callback);
  },

  create(rol, callback) {
    return service.create(rol, callback);
  },

  update(rol, id, callback) {
    return service.update(rol, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
