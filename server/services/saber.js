const GeneralService = require("./generalService");
const service = new GeneralService("saberes");
const db = require("../lib/database");

module.exports = {
  getAll(callback) {
    return service.getAll(callback);
  },

  getById(id, callback) {
    return service.getById(id, callback);
  },

  getLista(competenciaId, saberId, callback) {
    return db.query(
      `CALL prcSaberLista(${competenciaId}, ${saberId}, 0, 1000)`,
      callback
    );
  },

  getElemento(competenciaId, saberId, elementoId, callback) {
    return db.query(
      `CALL prcElementoSaber(${competenciaId}, ${saberId}, ${elementoId})`,
      callback
    );
  },

  getByCode(code, callback) {
    return service.getByCode(code, callback);
  },

  create(saber, callback) {
    return service.create(saber, callback);
  },

  update(saber, id, callback) {
    return service.update(saber, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
