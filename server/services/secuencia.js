const GeneralService = require("./generalService");
const service = new GeneralService("secuencia");
const db = require("../lib/database");

module.exports = {
  getAll(callback) {
    return service.getAll(callback);
  },

  getById(id, callback) {
    if (!callback) throw Error("Callback is necessary.");

    return db.query(
      {
        sql: `call prcSecuencia(${id})`
      },
      callback
    );
  },

  getByNivelAsignatura(nivelAsignaturaId, callback) {
    if (!callback) throw Error("Callback is necessary.");

    return db.query(
      {
        sql: `call prcSecuenciaLista(${nivelAsignaturaId}, 0, 1000)`
      },
      callback
    );
  },

  getByCode(code, callback) {
    return service.getByCode(code, callback);
  },

  create(competenciaGenerica, callback) {
    return service.create(competenciaGenerica, callback);
  },

  update(competenciaGenerica, id, callback) {
    return service.update(competenciaGenerica, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
