const GeneralService = require("./generalService");
const service = new GeneralService("secuencia");
const db = require("../lib/database");

module.exports = {
  get(subEjeId, callback) {
    if (!callback) throw Error("Callback is necessary.");

    return db.query(
      {
        sql: `call prcSeguimientoCompetencia(${subEjeId})`
      },
      callback
    );
  }
};
