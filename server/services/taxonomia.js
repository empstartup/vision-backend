const GeneralService = require("./generalService");
const service = new GeneralService("taxonomia");

module.exports = {
  getAll(callback) {
    return service.getAll(callback);
  },

  getById(id, callback) {
    return service.getById(id, callback);
  },

  getByCode(code, callback) {
    return service.getByCode(code, callback);
  },

  create(taxonomia, callback) {
    return service.create(taxonomia, callback);
  },

  update(taxonomia, id, callback) {
    return service.update(taxonomia, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
