const GeneralService = require("./generalService");
const service = new GeneralService("tipoensenanza");
const db = require("../lib/database");

module.exports = {
  getAll(callback) {
    return db.query("CALL prcTipoEnsenanzaLista()", callback);
  },

  getById(id, callback) {
    return service.getById(id, callback);
  },

  getByCode(code, callback) {
    return service.getByCode(code, callback);
  },

  create(tipoEnsenanza, callback) {
    return service.create(tipoEnsenanza, callback);
  },

  update(tipoEnsenanza, id, callback) {
    return service.update(tipoEnsenanza, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
