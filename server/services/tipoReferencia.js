const GeneralService = require("./generalService");
const service = new GeneralService("tiporeferencia");

module.exports = {
  getAll(callback) {
    return service.getAll(callback);
  },

  getById(id, callback) {
    return service.getById(id, callback);
  },

  getByCode(code, callback) {
    return service.getByCode(code, callback);
  },

  create(tipoReferencia, callback) {
    return service.create(tipoReferencia, callback);
  },

  update(tipoReferencia, id, callback) {
    return service.update(tipoReferencia, id, callback);
  },

  delete(id, callback) {
    return service.delete(id, callback);
  }
};
