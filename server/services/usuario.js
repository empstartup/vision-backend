const db = require("../lib/database");
const tableName = "usuario";

module.exports = {
  authenticate(username, callback) {
    if (!callback) throw Error("Callback is necessary.");

    return db.query(
      {
        sql: `SELECT Usuario, Clave, Empleado_ID FROM ${tableName} WHERE Usuario = ?`,
        values: username
      },
      callback
    );
  },

  getAll(callback) {
    return db.query(
      `SELECT Usuario, Estado, UltimoAcceso, Empleado_ID FROM ${tableName}`,
      callback
    );
  },

  getById(id, callback) {
    return db.query(
      `SELECT Usuario, Estado, UltimoAcceso, Empleado_ID FROM ${tableName} WHERE ${tableName}.ID = ?`,
      id,
      callback
    );
  },

  create(usuario, callback) {
    return db.query(`INSERT INTO ${tableName} SET ?`, usuario, callback);
  },

  update(usuario, id, callback) {
    return db.query(
      `UPDATE ${tableName} SET ? WHERE ID = ?`,
      [usuario, id],
      callback
    );
  },

  delete(id, callback) {
    return db.query(`DELETE FROM ${tableName} WHERE ID = ?`, id, callback);
  }
};
